import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class GameManager {	
	static Scanner inputDevice = new Scanner( System.in );
	static int maxAttemps = 10, wrongAttemps = 0, rounds = 0;
	static String word = "", sentenceOfPlay;
	static char[] lettersDiscovered;
	static ArrayList<Character> lettersFailed = new ArrayList();
	static Character letter = 0;
	//screen
	static char[][] GallowDraw = new char[6][6], GallowScreen = new char[6][6]; //La 1a es l'array amb el dibuix y la 2a la que es mostra per pantalla
	static String[] CoordinateDraw = new String[10];
	
	public static void main(String[] args) {
		establishDraw();
		establishCoordinates();
		init();
		LoopGame();
		WhatDoYouWant();
		return;
	}
	
	static void init() { //Serveix per posar a 0 les variables, pinta en blanc l'array de pantalla i per establir la paraula aleatoria
		wrongAttemps = 0;
		rounds = 0;
		letter = 0;
		lettersFailed.clear();
		establishWord();
		establishScreen();
	}
	
	static void establishWord() {	//En aquesta funcio s'escolleix una paraula aleatoria del txt
		System.out.println();
		System.out.println("PRACTICA 2 - PENJAT");
		ArrayList<String> wordsList = readWords.readWordsOnTxt("words.txt");
		double rnd = Math.random() * wordsList.size();
		word = wordsList.get((int) rnd);
		lettersDiscovered = new char[word.length()];
		for (int i = 0; i < lettersDiscovered.length; i++) {
			lettersDiscovered[i] = '*';
		}
	}
	
	static void establishDraw() { //En aquesta funcio es dibuixa el penjat en l'array
		for (int y = 0; y < 5; y++) {
			GallowDraw[0][y] = '|';
		}
		for (int x = 0; x < 4; x++) {
			GallowDraw[x][5] = '_';
		}
		for (int x = 1; x < 6; x++) {
			GallowDraw[x][0] = '_';
		}
		GallowDraw[3][4] = 'o';
		GallowDraw[3][3] = 'O';
		GallowDraw[3][2] = '�';
		GallowDraw[2][3] = '/';
		GallowDraw[4][3] = '\\';
		GallowDraw[2][2] = '/';
		GallowDraw[4][2] = '\\';
	}
	
	static void establishScreen() { //En aquesta funcio es buida el penjat que es mostrara per pantalla
		for (int x = 0; x < 6; x++) {
			for (int y = 0; y < 6; y++) {
				GallowScreen[x][y] = ' ';
			}
		}
	}
	
	static void establishCoordinates() { //En aquesta funci� s'estableixen les coordenades de l'array que es tenen que dibuixar a cada error
		CoordinateDraw[0] = "1,0:2,0:3,0:4,0:5,0";
		CoordinateDraw[1] = "0,0:0,1:0,2:0,3:0,4";
		CoordinateDraw[2] = "0,5:1,5:2,5:3,5";
		CoordinateDraw[3] = "3,4:";
		CoordinateDraw[4] = "2,3:";
		CoordinateDraw[5] = "3,3:";
		CoordinateDraw[6] = "4,3:";
		CoordinateDraw[7] = "2,2:";
		CoordinateDraw[8] = "4,2:";
		CoordinateDraw[9] = "3,2:";
	}
	
	public static void LoopGame() { //Bucle del joc
		boolean end = false;
		while (!end) {
			ShowDrawInScreen(GallowScreen);
			RoundMsg();
			NewLetterManager();
			if (allFound()) {
				end = true;
				VictoryMsg();
			} else if (wrongAttemps >= maxAttemps) {
				end = true;
				DefeatMsg();
			}
			System.out.println();
		}
	}
	
	static void ShowDrawInScreen(char[][] draw) {		//Aquesta funcio mostra per pantalla l'array que pasem per parametre
		for (int y = 5; y >= 0; y--) {
			for (int x = 0; x < 6; x++) {
				System.out.print(draw[x][y]);
			}
			System.out.println();
		}
	}
	
	static void UpdateScreenDraw() { //Aquesta funcio actualitza el dibuix del penjat
		String[] split1 = CoordinateDraw[wrongAttemps-1].split(":");
		for (int i = 0; i < split1.length; i++) {
			String[] split2 = split1[i].split(",");
			int[] split2int = {Integer.parseInt(split2[0]), Integer.parseInt(split2[1])};
			GallowScreen[split2int[0]][split2int[1]] = GallowDraw[split2int[0]][split2int[1]];
		}
	}
	
	static void RoundMsg() { //Aquesta funcio mostra els misatges de cada ronda
		rounds++;
		if (letter != 0) {
			System.out.println();
			System.out.println(sentenceOfPlay);
		}		
		System.out.println("Intents que et queden: "+(maxAttemps-wrongAttemps));
		if (!lettersFailed.isEmpty()) {
			System.out.print("Lletres que has fallat: ");
			for (int i = 0; i < lettersFailed.size(); i++) {
				System.out.print(lettersFailed.get(i));
				if (i < lettersFailed.size() - 1) {
					System.out.print(", ");
				}
			}
			System.out.println();
		}		
		System.out.println("Paraula: "+String.valueOf(lettersDiscovered));
		inputNewLetter();
		System.out.println();
	}
	
	static void inputNewLetter() { //En aquesta funcio s'introdueix la lletra
		boolean error = true;
		while (error) {
			System.out.print("Escriu la seg�ent lletra que creus que est� a la paraula: ");
			letter = inputDevice.next().charAt(0);
			letter = Character.toLowerCase(letter); //Si la lletra es majuscula, es converteix a minuscula
			boolean b = false;
			for (int i = 0; i < lettersDiscovered.length; i++) {	//Si la lletra ja l'haviem dit dona error
				if (lettersDiscovered[i] == letter) {
					System.out.println("ERROR! Aquesta lletra ja l'havies dit!");
					b = true;
					break;
				}
			}
			if (!b) {
				for (int i = 0; i < lettersFailed.size(); i++) {	//Si la lletra ja l'haviem dit dona error
					if (lettersFailed.get(i) == letter) {
						System.out.println("ERROR! Aquesta lletra ja l'havies dit!");
						b = true;
						break;
					}
				}
			}
			if (!b) {	//Aqui es mira que la lletra sigui una lletra y no sigui una � (no hi ha cap paraula en catala que tingui �)
				if (Character.isLetter(letter) && !letter.equals('�')) { error = false; }
				else { System.out.println("ERROR! Nom�s tens que introduit lletres (la � no esta permesa)!"); }
			}			
		}		
	}
	
	static void NewLetterManager() { //Aqui es guarda un misstage seguons si la lletra es correcta o no
		int _FoundLetter = FoundLetter();
		if (_FoundLetter > 0) {
			if (_FoundLetter == 1) {
				sentenceOfPlay = "Enhorabona! La lletra \""+letter+"\" es correcta";
			} else {
				sentenceOfPlay = "Enhorabona! La lletra \""+letter+"\" es correcta. Has encertat "+_FoundLetter+" lletres alhora!";
			}
		} else {
			sentenceOfPlay = "La lletra \""+letter+"\" que has introduit no es correcta, has perdut un intent!";
			wrongAttemps++;
			lettersFailed.add(letter);
			UpdateScreenDraw();
		}			
	}
	
	static int FoundLetter() {	//Aquesta funcio mira si la lletra es troba a la paraula y retorna el n� de vegades que s'ha trobat
		int _FoundLetter = 0;
		for (int i = 0; i < word.length(); i++){
			if (letter == word.charAt(i)) {
				lettersDiscovered[i] = letter;
				_FoundLetter++;
			}
		}
		return _FoundLetter;
	}
	
	static boolean allFound() { //Aquesta funcio retorna true si s'ha endevinat tota la paraula
		for (int i = 0; i < word.length(); i++) {
			if (word.charAt(i) != lettersDiscovered[i]) {
				return false;
			}
		}
		return true;
	}
	
	static void VictoryMsg() { //Funcio amb el missatge de victoria
		System.out.println();
		ShowDrawInScreen(GallowScreen);
		System.out.println("Paraula: "+String.valueOf(lettersDiscovered));
		System.out.println("Enhorabona! Has aconsegit guanyar la partida!");
		int numAttemps = maxAttemps-wrongAttemps;
		System.out.println("T'han sobrat "+numAttemps+" intent/s");
		System.out.println("Has necessitat "+rounds+" rondes per guanyar el nivell");
	}
	
	static void DefeatMsg() {	//Funcio amb el misatge de derrota
		System.out.println();
		ShowDrawInScreen(GallowScreen);
		System.out.println("Ho sento! No has aconseguit descobrir la paraula");
		System.out.println("La paraula era "+word);
		System.out.println("Has estat jugant durant "+rounds+" rondes");
	}
	
	static void WhatDoYouWant() {	//Funcio que un cop has acabat el joc, et pregunta si vols tornar a jugar o sortir
		System.out.println();
		System.out.print("Escriu qualsevol lletra/n�mero per tornar a jugar o \"sortir\" per sortir: ");
		String a = inputDevice.next();
		a = a.toLowerCase();
		if (!a.equals("sortir")) {
			System.out.println();
			init();
			LoopGame();
			WhatDoYouWant();
		}
	}
	
	public static class readWords {	//Inner class amb una funcio que retorna una llista amb totes les paraules d'un fitxer
		
		public static ArrayList<String> readWordsOnTxt(String filename) {
			ArrayList<String> wordsList = new ArrayList(); 
			File file = new File(filename);
			try {
				FileInputStream fstream = new FileInputStream(file);
				DataInputStream dstream = new DataInputStream(fstream);
				BufferedReader breader = new BufferedReader(new InputStreamReader (dstream));
				String strLine;
				while ((strLine = breader.readLine()) != null) {
					wordsList.add(strLine);
				}
				dstream.close();
			} catch (FileNotFoundException e) {
				System.out.println("ERROR!");
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("ERROR!");
				e.printStackTrace();
			}			
			return wordsList;
		}
	}
}